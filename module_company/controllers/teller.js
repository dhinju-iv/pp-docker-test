var network = require("../library/network/index");
var util = require("../util/index");

function createSeqno(seqtype){
    return new Promise ((resolve, reject) => {
        let resSeqNo = network.getNextSequence(seqtype)
        if(resSeqNo){
            resolve(resSeqNo)
        }else{
            reject('error')
        }        
    })
}

function searchStore(params, logicalOp, isCustomQuery){
    return new Promise ((resolve, reject) => {
        let response = network.getStoreSearchResponse(params, logicalOp, isCustomQuery)
        if(response){
            resolve(response)
        }else{
            reject('error')
        }        
    })
} 

function createTellerFunc(teller, resSeqNo){
    return new Promise ((resolve, reject) => {
        if (resSeqNo.status) {
            teller.isActive = true;
            teller.tellerId = resSeqNo.data;
            resolve(teller);
        } else {
            reject(resSeqNo);
        }
    })
    
}

function saveTellerFunc(teller, message, isUpdate){
    return new Promise ((resolve, reject) => {
        var indexName = isUpdate ? "tellerId" : undefined;  
        resolve({teller:teller, isUpdate:isUpdate, message:message}) 
    });    
}

function countTellerFunc(params, logicalOp, isCustomQuery){
    return new Promise ((resolve, reject) => {
        var logicalOp1 = typeof logicalOp !== 'undefined' ? logicalOp : "$and",
        isCustomQuery1 = typeof isCustomQuery !== 'undefined' ? isCustomQuery : false,
            queryParts = [],
            searchQuery = {};

        for (var key in params) {
            queryParts = util.buildQueryParts(queryParts, key, params[key]);
        }

        if (isCustomQuery1) {
            queryParts.push(params.query)
        }

        if (Object.keys(params).length > 0 && queryParts.length > 0) {
            searchQuery[logicalOp1] = queryParts;
        }
        resolve(searchQuery)
    })
}

function searchTellerFunc(params, logicalOp, isCustomQuery){
    return new Promise ((resolve, reject) => {       
        var logicalOp1 = typeof logicalOp !== 'undefined' ? logicalOp : "$and",
            isCustomQuery1 = typeof isCustomQuery !== 'undefined' ? isCustomQuery : false,
            isNext = typeof params.isNext != 'undefined' ? params.isNext : true,
            sortBy = typeof params.sortBy != 'undefined' ? params.sortBy : "tellerId",
            sortOrder = typeof params.sortOrder != 'undefined' ? params.sortOrder : "ASC",
            queryParts = [],
            options = { docType: 1 },
            singleDocumentKeys = ["tellerId"],
            searchQuery = {},
            that = this;

        if (isCustomQuery1) {
            queryParts.push(params.query)
        }
        
        for (var key in params) {
            queryParts = util.buildQueryParts(queryParts, key, params[key]);
            if (singleDocumentKeys.includes(key)) {
                options.docType = 0;
            }
        }

        if (params.lastValue && params.lastIndex) {
            let comparisonOp = (isNext && sortOrder === "ASC" || !isNext && sortOrder === "DESC") ? '$gt' : '$lt';
            queryParts.push({
                $or: [
                    { [sortBy]: { [comparisonOp]: params.lastValue } },
                    { [sortBy]: params.lastValue, tellerId: { [comparisonOp]: params.lastIndex } }
                ]
            })
        } else if (params.lastIndex) {
            queryParts.push({ tellerId: { [comparisonOp]: params.lastIndex } });
        }

        if (Object.keys(params).length > 0 && queryParts.length > 0) {
            searchQuery[logicalOp1] = queryParts;
        }

        var aggregateQuery = [];
        
        aggregateQuery.push({ "$match": searchQuery });

        if (options.docType === 1) {
            sortOrder = (isNext && sortOrder === "ASC" || !isNext && sortOrder === "DESC") ? 1 : -1;
            var sort = { [sortBy]: sortOrder };
            sort.companyId = sortOrder;
            aggregateQuery.push({ "$sort": sort });
            var limit = (typeof params.pageSize !== "undefined") ? params.pageSize : 10;
            aggregateQuery.push({ "$limit": limit });
        }
        resolve({aggregateQuery:aggregateQuery, options:options, isNext:isNext})
    })
}

function makedataservicerequestfunction(query, operation, table, method, selector, options, keyIdentifier){
    return new Promise ((resolve, reject) => {
        var indexName = keyIdentifier ? "tellerId" : undefined; 
        let makedataservicerequestnetworkres = network.makedataservicerequestnetwork(query, operation, table, method, selector, options, indexName)
        if(makedataservicerequestnetworkres){
            resolve(makedataservicerequestnetworkres)
        }else{
            reject('error')
        }        
    })
}

function createLogFunc(log){
    return new Promise ((resolve, reject) => {
        let logResponse = network.makeLogRequestNetwork(log);
        resolve(logResponse);
    })
}

module.exports = {    
    searchTeller : async (params, logicalOp, isCustomQuery) => {
        let searchCompanyResponse = await searchTellerFunc(params, logicalOp, isCustomQuery) 
        if(searchCompanyResponse){
            let makedataservicerequestfunctionres = await makedataservicerequestfunction(searchCompanyResponse.aggregateQuery, "aggregate", "TellerMaster", "POST", null, searchCompanyResponse.options, false)
            if (makedataservicerequestfunctionres.status) {   
                createLogFunc({data:{message: "teller search: success", module: "teller", collectionId: null, createdBy: "admin", info: params}});             
                if ((Array.isArray(makedataservicerequestfunctionres.data) && makedataservicerequestfunctionres.data.length === 0) || makedataservicerequestfunctionres.data === null) {
                    return({ status: false, data: null, error: "Teller not Found" });
                } else {                                 
                    if (!searchCompanyResponse.isNext && Array.isArray(makedataservicerequestfunctionres.data)) {                        
                        makedataservicerequestfunctionres.data = makedataservicerequestfunctionres.data.reverse();
                    }
                    return(makedataservicerequestfunctionres)                    
                }
            } else {
                createLogFunc({data:{message: "teller search: error", module: "teller", collectionId: null, createdBy: "admin", info: makedataservicerequestfunctionres}});
                return(makedataservicerequestfunctionres);
            }
        }        
    },

    countTeller: async (params, logicalOp, isCustomQuery) => {
        let countCompanyResponse = await countTellerFunc(params, logicalOp, isCustomQuery)
        if(countCompanyResponse){
            let makedataservicerequestfunctionres = await makedataservicerequestfunction(countCompanyResponse, "count", "TellerMaster", "POST", { projection: { _id: 0 } }, {}, false)
            createLogFunc({data:{message: "count teller", module: "teller", collectionId: null, createdBy: "admin", info: null}});  
            return makedataservicerequestfunctionres
        }        
    },
  
    updateTeller: async(company) => {
        company = util.formatCompanyParams(company);
        let saveresponse = await saveTellerFunc(company, "Teller Updated Successfully", true)
        if(saveresponse){
            let makedataservicerequestres = await makedataservicerequestfunction(saveresponse.teller, "write", "TellerMaster", "POST", null, { docType: 0 }, true)
            if(makedataservicerequestres.status){
                createLogFunc({data:{message: "update teller: success", module: "teller", collectionId: company.tellerId, createdBy: "admin", info: saveresponse.teller}});  
                return({ status: true, data: { message: saveresponse.message, tellerId: company.tellerId }, error: null });
            }else{
                createLogFunc({data:{message: "update teller: error", module: "teller", collectionId: company.tellerId, createdBy: "admin", info: makedataservicerequestres}});
                return makedataservicerequestres
            }            
        } 
    },
    
    setTellerStatus: async (company) => {
        let saveresponse = await saveTellerFunc(company, "Teller Status Changed Successfully", true) 
        if(saveresponse){
            let makedataservicerequestres = await makedataservicerequestfunction(saveresponse.teller, "write", "TellerMaster", "POST", null, { docType: 0 }, true)
            if(makedataservicerequestres.status){
                createLogFunc({data:{message: "set teller status: success", module: "teller", collectionId: company.tellerId, createdBy: "admin", info: null}});
                return({ status: true, data: { message: saveresponse.message, tellerId: company.tellerId }, error: null });
            }else{
                createLogFunc({data:{message: "set teller status: error", module: "teller", collectionId: company.tellerId, createdBy: "admin", info: makedataservicerequestres}});
                return makedataservicerequestres
            }            
        }        
    },
    
    createTeller: async (teller) => {
        let resSeqNo = await createSeqno('TellerMaster')
        if(resSeqNo){
            let response = await createTellerFunc(teller, resSeqNo)
            if(response){
                let saveresponse = await saveTellerFunc(response, "Teller Created Successfully", false)
                if(saveresponse){
                    let makedataservicerequestresponse = await makedataservicerequestfunction(saveresponse.teller, "write", "TellerMaster", "POST", null, { docType: 0 }, saveresponse.isUpdate)
                    if (makedataservicerequestresponse.status) {
                        createLogFunc({data:{message: "create teller : success", module: "teller", collectionId: teller.tellerId, createdBy: "admin", info: null}});
                        return({ status: true, data: { message: saveresponse.message, tellerId: teller.tellerId }, error: null });
                    } else {
                        createLogFunc({data:{message: "create teller : error", module: "teller", collectionId: teller.tellerId, createdBy: "admin", info: makedataservicerequestresponse}});
                        return(makedataservicerequestresponse);
                    }
                }                
            }  
        }else{
            return ({ status: false, data: null, error: "Can't connect to Data service" });
        }
              
    },

    searchTellerStore: async(params, logicalOp, isCustomQuery) => {
        let response = await searchStore(params, logicalOp, isCustomQuery)
        return response;
    },


}