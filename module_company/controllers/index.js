var network = require("../library/network/index");

function createCompanyFunc(company){
    return new Promise ((resolve, reject) => {
        let date_ob = new Date();
        let date = ("0" + date_ob.getDate()).slice(-2);
        let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
        let year = date_ob.getFullYear();
        let hours = date_ob.getHours();
        let minutes = date_ob.getMinutes();
        let seconds = date_ob.getSeconds();
        company.isActive = true;
        company.createdAt = year+'-'+month+'-'+date+' '+hours+':'+minutes+':'+seconds;
        resolve(company);
    })
    
}

function saveCompanyFunc(company, message, isUpdate){
    return new Promise ((resolve, reject) => {
        var indexName = isUpdate ? "companyId" : undefined;  
        resolve({company:company, isUpdate:indexName, message:message}) 
    });    
}

function makedataservicerequestfunction(query, operation, table, method, selector, options, keyIdentifier){
    return new Promise ((resolve, reject) => {
        var indexName = keyIdentifier ? "companyId" : undefined; 
        let makedataservicerequestnetworkres = network.makedataservicerequestnetwork(query, operation, table, method, selector, options, indexName)
        if(makedataservicerequestnetworkres){
            resolve(makedataservicerequestnetworkres)
        }else{
            reject('error')
        }        
    })
}


module.exports = {    
    
    createCompany: async (company) => {        
        let response = await createCompanyFunc(company)
        if(response){
            let saveresponse = await saveCompanyFunc(response, "Company Created Successfully", false)
            if(saveresponse){
                let makedataservicerequestresponse = await makedataservicerequestfunction(saveresponse.company, "write", "CompanyMaster", "POST", null, { docType: 0 }, saveresponse.isUpdate)
                if (makedataservicerequestresponse.status) {
                    return({ status: true, data: { message: saveresponse.message, companyId: company.companyId }, error: null });
                } else {
                    return(makedataservicerequestresponse);
                }
            }                
        }  
    }
}