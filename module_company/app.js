var express = require('express'),
    bodyParser = require('body-parser');

var indexRouter = require('./routes/index');

var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/', indexRouter);

app.use(function (req, res, next) {
    return res.status(404).send({ status: false, data: null, error: "Invalid Path" });
});

app.use((err, req, res, next) => {
    if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
        return res.status(400).send({ status: false, data: null, error: "Bad request" });
    }
    next();
});

var port = 3011;
app.set('port', port);
app.listen(port);

module.exports = app