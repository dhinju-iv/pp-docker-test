var express = require('express');
const { check, oneOf, validationResult } = require('express-validator/check');
var router = express.Router();
var controller = require("../controllers/index");
var util = require("../util/index");

router.post('/create', [
    check('companyAddress').isString().withMessage('Please provide an address').not().isEmpty().withMessage('Please provide an address').trim(),
    check('bankRouteCode').isString().withMessage('Please provide a valid bank route code').not().isEmpty().withMessage('Please provide a valid bank route code').trim(),
    check('bankAccountNumber').isString().withMessage('Please provide a valid bank account number').not().isEmpty().withMessage('Please provide a valid bank account number').trim(),
    check('phoneNumber').isString().withMessage('Please provide a valid phone number').not().isEmpty().withMessage('Please provide a valid phone number').trim(),
    check("companyId").isEmpty().withMessage("Invalid input parameter").trim()
], function (req, res, next) {
    try {
        validationResult(req).throw();
        var ipCompany = req.body;
        controller.createCompany(ipCompany).then((company) => {
            res.send(company);
        });
    } catch (err) {
        var errorMsgs = util.formatValidationErrors(err);
        res.send({ status: false, data: null, error: { validationErrors: errorMsgs } });
    }
});


module.exports = router;