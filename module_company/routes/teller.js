var express = require('express');
const { check, oneOf, validationResult } = require('express-validator/check');
var router = express.Router();
var controller = require("../controllers/teller");
var util = require("../util/index");
var validatorHelper = require("../util/validatorHelper");

router.post('/search', [
    check('firstName').isString().withMessage('Please provide a valid firstName').not().isEmpty().withMessage('Please provide a firstName').trim().optional(),
    check('lastName').isString().withMessage('Please provide a valid lastName').not().isEmpty().withMessage('Please provide a lastName').trim().optional(),
    check('tellerInitial').isString().withMessage('Please provide a valid initial').not().isEmpty().withMessage('Please provide a valid initial').trim().optional(),
    check('tellerId').isString().withMessage('Please provide a valid Teller ID').not().isEmpty().withMessage('Please provide a Teller ID').trim().optional(),
    check('tellerEmail').isEmail().withMessage('Please provide a valid Teller Email').optional(),
    check('isActive').custom(value => typeof value === "boolean").withMessage('Please provide a valid Active flag').optional(),
    check("searchQuery").isString().withMessage('Please provide a valid search query').not().isEmpty().withMessage("Please provide a search query").trim().optional(),
    check('lastIndex').isString().withMessage('Please provide a valid Teller ID').not().isEmpty().withMessage('Please provide a Teller ID').trim().optional(),
    check('lastValue').isString().withMessage('Please provide a valid value').not().isEmpty().withMessage('Please provide a value').trim().optional(),
    check('pageSize').isInt().withMessage('Please provide a valid Page Size').toInt().optional(),
    check('isNext').custom(value => typeof value === "boolean").withMessage('Please provide a valid value').optional(),
    check('sortBy').isIn(["tellerId", "firstName"]).withMessage('Please provide a valid value').optional(),
    check('sortOrder').isIn(["ASC", "DESC"]).withMessage('Please provide a valid value').optional(),
], function (req, res, next) {
    try {
        validationResult(req).throw();
        var ipCompany = req.body,
            isCustomQuery = false,
            query = {};

        query = ipCompany;
        if (ipCompany.hasOwnProperty("searchQuery")) {
            query.query = util.buidTellerCustomSearchQuery(ipCompany.searchQuery);
            isCustomQuery = true;
        }
        controller.searchTeller(query, "$and", isCustomQuery).then((response) => {
            res.send(response);
        });
    } catch (err) {
        var errorMsgs = util.formatValidationErrors(err);
        res.send({ status: false, data: null, error: { validationErrors: errorMsgs } });
    }
});

router.post('/getCount', [
    check('tellerInitial').isString().withMessage('Please provide a valid Initial').not().isEmpty().withMessage('Please provide a valid Initial').trim().optional(),
    check('isActive').custom(value => typeof value === "boolean").withMessage('Please provide a valid Active flag').optional(),
    check("searchQuery").isString().withMessage('Please provide a valid search query').not().isEmpty().withMessage("Please provide a search query").trim().optional(),
], function (req, res, next) {
    try {
        validationResult(req).throw();
        var ipCompany = req.body,
            isCustomQuery = false,
            query = {};

        query = ipCompany
        if (ipCompany.searchQuery) {
            query.query = util.buidTellerCustomSearchQuery(ipCompany.searchQuery);
            isCustomQuery = true;
        }
        controller.countTeller(query, "$and", isCustomQuery).then((count) => {
            res.send(count);
        });
    } catch (err) {
        var errorMsgs = util.formatValidationErrors(err);
        res.send({ status: false, data: null, error: { validationErrors: errorMsgs } });
    }
});

router.post('/create', [
    check('firstName').isString().withMessage('Please provide a valid first name').not().isEmpty().withMessage('Please provide a first name').trim(),
    check('lastName').isString().withMessage('Please provide a valid last name').not().isEmpty().withMessage('Please provide a last name').trim(),
    check('tellerInitial').isString().withMessage('Please provide a valid initial').not().isEmpty().withMessage('Please provide a valid initial').trim(),
    check('tellerEmail').isEmail().withMessage('Please provide a valid email address').custom(value => {
        return validatorHelper.isTellerEmailExists(value).then((message) => { if (message) return Promise.reject(message); })
    }),
    check('tellerStore').custom((value, { req }) => {
        return validatorHelper.isValidTellerStore(value, req).then((message) => { if (message) return Promise.reject(message); })
    }),
    check("tellerId").isEmpty().withMessage("Invalid input parameter").trim()
], function (req, res, next) {
    try {
        validationResult(req).throw();
        var ipCompany = req.body;
        controller.createTeller(ipCompany).then((company) => {
            res.send(company);
        });
    } catch (err) { 
        var errorMsgs = util.formatValidationErrors(err);
        res.send({ status: false, data: null, error: { validationErrors: errorMsgs } });
    }
});

router.post('/setTellerStatus', [
    check('tellerId').custom((value, { req }) => {
        return validatorHelper.isValidTellerId(value, req).then((message) => { if (message) return Promise.reject(message); })
    }).trim(),
    check('isActive').custom(value => typeof value === "boolean").withMessage('Please provide a valid Active flag')
], function (req, res, next) {
    try {
        validationResult(req).throw();
        var ipCompany = req.body;
        var company = {
            tellerId: ipCompany.tellerId,
            isActive: ipCompany.isActive
        }
        controller.setTellerStatus(company).then((response) => {
            res.send(response);
        });
    } catch (err) {
        var errorMsgs = util.formatValidationErrors(err);
        res.send({ status: false, data: null, error: { validationErrors: errorMsgs } });
    }
});

router.post('/update', [
    check('tellerId').custom((value, { req }) => {
        return validatorHelper.isValidTellerId(value, req).then((message) => { if (message) return Promise.reject(message); })
    }).trim(),
    check('firstName').isString().withMessage('Please provide a valid first name').not().isEmpty().withMessage('Please provide a first name').trim().optional(),
    check('lastName').isString().withMessage('Please provide a valid last name').not().isEmpty().withMessage('Please provide a last name').trim().optional(),
    check('tellerInitial').isString().withMessage('Please provide a valid initial').not().isEmpty().withMessage('Please provide a valid initial').trim().optional(),
    check('tellerEmail').isEmail().withMessage('Please provide a valid email address').custom((value, { req }) => {
        return validatorHelper.isTellerEmailExists(value, req).then((message) => { if (message) return Promise.reject(message); })
    }).optional(),
    check('tellerStore').custom((value, { req }) => {
        return validatorHelper.isValidTellerStore(value, req).then((message) => { if (message) return Promise.reject(message); })
    }).optional(),
], function (req, res, next) {
    try {
        validationResult(req).throw();
        var ipCompany = req.body;
        controller.updateTeller(ipCompany).then((response) => {
            res.send(response);
        });        
    } catch (err) {
        var errorMsgs = util.formatValidationErrors(err);
        res.send({ status: false, data: null, error: { validationErrors: errorMsgs } });
    }
});



module.exports = router;