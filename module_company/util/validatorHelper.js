var controller = require("../controllers/index");
var tellerController = require("../controllers/teller");
const network = require("../library/network/index");

module.exports = {
    isCompanyNameExists: function (companyName, req) {
        return new Promise((resolve, reject) => {
            try {
                if (typeof companyName !== "string") return resolve("Please provide a string value");
                if (companyName === "") return resolve("Please provide a company name");
                if (req && req.company && req.company.companyName === companyName) {
                    return resolve(false);
                }
                controller.searchCompany({ companyName: companyName }, undefined).then((searchRes) => {
                    if (searchRes.status && searchRes.data) {
                        return resolve("Company name exists");
                    } else {
                        return resolve(false);
                    }
                });
            } catch (error) {
                return resolve("Some error occured");
            }
        });
    },
    isTellerNameExists: function(tellerName, req){
        return new Promise((resolve, reject) => {
            try{
                if (typeof tellerName !== "string") return resolve("Please provide a string value");
                if (tellerName === "") return resolve("Please provide a teller name");
                if (req && req.company && req.company.tellerName === tellerName) {
                    return resolve(false);
                }
                tellerController.searchTeller({ tellerName: tellerName }, undefined).then((searchRes) => {
                    if (searchRes.status && searchRes.data) {
                        return resolve("Teller name exists");
                    } else {
                        return resolve(false);
                    }
                });
            }catch(error){
                return resolve("Some error occured");
            }
        });
    },
    isValidCompanyId: function (companyId, req) {
        return new Promise((resolve, reject) => {
            try {
                if (typeof companyId !== "string") return resolve("Please provide a string value");
                if (companyId === "") return resolve("Please provide a company id");
                controller.searchCompany({ companyId: companyId }, undefined).then((searchRes) => {
                    if (searchRes.status && searchRes.data) {
                        req["company"] = searchRes.data;
                        return resolve(false);
                    } else {
                        return resolve("Company id not found");
                    }
                });
            } catch (error) {
                return resolve("Some error occured");
            }
        });
    },
    isValidTellerId: function (tellerId, req) {
        return new Promise((resolve, reject) => {
            try {
                if (typeof tellerId !== "string") return resolve("Please provide a string value");
                if (tellerId === "") return resolve("Please provide a teller id");
                tellerController.searchTeller({ tellerId: tellerId }, undefined).then((searchRes) => {
                    if (searchRes.status && searchRes.data) {
                        req["company"] = searchRes.data;
                        return resolve(false);
                    } else {
                        return resolve("Teller id not found");
                    }
                });
            } catch (error) {
                return resolve("Some error occured");
            }
        });
    },
    isCompanyEmailExists: function (companyEmail, req) {
        return new Promise((resolve, reject) => {
            try {
                if (typeof companyEmail !== "string") return resolve("Please provide a company email");
                if (companyEmail === "") return resolve("Please provide a company email");
                if (req && req.company && req.company.companyEmail === companyEmail) {
                    return resolve(false);
                }
                controller.searchCompany({ companyEmail: companyEmail }, undefined).then((searchRes) => {
                    if (searchRes.status && searchRes.data) {
                        return resolve("company email exists");
                    } else {
                        return resolve(false);
                    }
                });
            } catch (error) {
                return resolve("Some error occured");
            }
        });
    },
    isTellerEmailExists: function (tellerEmail, req) {
        return new Promise((resolve, reject) => {
            try {
                if (typeof tellerEmail !== "string") return resolve("Please provide a teller email");
                if (tellerEmail === "") return resolve("Please provide a teller email");
                if (req && req.company && req.company.tellerEmail === tellerEmail) {
                    return resolve(false);
                }
                tellerController.searchTeller({ tellerEmail: tellerEmail }, undefined).then((searchRes) => {
                    if (searchRes.status && searchRes.data) {
                        return resolve("Teller email exists");
                    } else {
                        return resolve(false);
                    }
                });
            } catch (error) {
                return resolve("Some error occured");
            }
        });
    },
    isValidVendorEndPoints: function (endPoints, req) {
        return new Promise((resolve, reject) => {
            try {
                if (!Array.isArray(endPoints)) return resolve("Please provide array value");
                if (endPoints.length === 0) return resolve("Please provide vendor end points");
                network.searchVendors({ endPointIds: endPoints }).then((searchRes) => {
                    if (searchRes.status && searchRes.data.length === endPoints.length && searchRes.data.every((endPoint) => endPoint.isActive)) {
                        return resolve(false);
                    } else {
                        return resolve("Invalid end points");
                    }
                });               
            } catch (error) {
                return resolve("Some error occured");
            }
        });
    },
    isValidTellerStore: function (store, req) {
        return new Promise((resolve, reject) => {
            try {
                if (!Array.isArray(store)) return resolve("Please provide array value");
                if (store.length === 0) return resolve("Please provide store");
                tellerController.searchTellerStore({ storeIds: store, pageSize: store.length }, undefined, true).then((searchRes) => {
                    if(searchRes.status && searchRes.data){
                        if(store.length === searchRes.data.length){
                            return resolve(false);  
                        }else{
                            return resolve("Invalid store"); 
                        }
                    }else{
                        return resolve("Invalid store");
                    }  
                });
            } catch (error) {
                return resolve("Some error occured");
            }
        });
    },

    isProviderIdExists: function (providerid, req) {
        return new Promise((resolve, reject) => {
            try {
                if (typeof providerid !== "string") return resolve("Please provide a string value");
                if (providerid === "") return resolve("Please provide a providerId");
                network.searchProvider(providerid).then((searchRes) => {
                    if (searchRes.status && searchRes.data) {
                        return resolve(false);
                    } else {
                        return resolve("ProviderId not found");
                    }
                });
            } catch (error) {
                return resolve("Some error occured");
            }
        });
    },

    isValidServiceId: function (serviceid, req){
        return new Promise((resolve, reject) => {
            try {
                if (!req.body.providerId || req.body.providerId === "") return resolve("Please provide a providerId");
                if (!Array.isArray(serviceid)) return resolve("Please provide array value");
                if (serviceid.length === 0) return resolve("Please provide service");                
                network.searchProvider(req.body.providerId).then((searchRes) => {
                    if (searchRes.status && searchRes.data) {
                        serviceid.map(function(service){
                            if(!searchRes.data.providerServices.includes(service)){
                                return resolve("Provider service not found:"+service+"");
                            } 
                        } ,searchRes);                        
                        return resolve(false);                        
                    }else{
                        return resolve("ProviderId not found");
                    }
                });
            }catch(error){
                return resolve("Some error occured");
            }
        });
    },
}
