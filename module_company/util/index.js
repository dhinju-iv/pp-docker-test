module.exports = {
    formatValidationErrors: function (err) {
        try {
            var errors = err.array();
            var errorMsgs = {};
            for (var i in errors) {
                if (errors[i].msg && !errors[i].nestedErrors) {
                    if (errors[i].param.indexOf(".") > -1) {
                        this.stringToObj(errors[i].param, errors[i].msg, errorMsgs);
                    } else {
                        errorMsgs[errors[i].param] = errors[i].msg;
                    }
                }
                if (errors[i].nestedErrors) {
                    for (var j in errors[i].nestedErrors) {
                        if (errors[i].nestedErrors[j].param.indexOf(".") > -1) {
                            this.stringToObj(errors[i].nestedErrors[j].param, errors[i].nestedErrors[j].msg, errorMsgs);
                        } else {
                            errorMsgs[errors[i].nestedErrors[j].param] = errors[i].nestedErrors[j].msg;
                        }
                    }
                }
            }
            return errorMsgs;
        } catch (err) {
            return err;
        }
    },
    stringToObj: function (path, value, obj) {
        var parts = path.split("."), part;
        var last = parts.pop();
        while (part = parts.shift()) {
            if (typeof obj[part] != "object") obj[part] = {};
            obj = obj[part];
        }
        obj[last] = value;
    },
    buildQueryParts: function (queryParts, key, value) {
        let regExpKeys = [];
        let noRegExpKeys = ["companyId", "companyName", "companyEmail", "isActive", "tellerId", "firstName", "lastName", "tellerInitial", "tellerEmail", "companyAddress", "bankRouteCode", "bankAccountNumber", "phoneNumber", "createdAt"];
        if (regExpKeys.includes(key)) {
            queryParts.push({ [key]: { $regex: value + ".*", $options: "i" } });
        } else if (noRegExpKeys.includes(key)) {
            queryParts.push({ [key]: value });
        }
        return queryParts;
    },
    buidCustomSearchQuery: function (searchQuery) {
        return {
            "$or": [{ companyId: { $regex: searchQuery + ".*", $options: "i" } },
            { companyName: { $regex: searchQuery + ".*", $options: "i" } },
            { companyAddress: { $regex: searchQuery + ".*", $options: "i" } },
            { bankRouteCode: { $regex: searchQuery + ".*", $options: "i" } },
            { bankAccountNumber: { $regex: searchQuery + ".*", $options: "i" } },
            { phoneNumber: { $regex: searchQuery + ".*", $options: "i" } },
            { createdAt: { $regex: searchQuery + ".*", $options: "i" } }]
        };
    },
    buidTellerCustomSearchQuery: function (searchQuery) {
        return {
            "$or": [{ tellerId: { $regex: searchQuery + ".*", $options: "i" } },
            { firstName: { $regex: searchQuery + ".*", $options: "i" } },
            { lastName: { $regex: searchQuery + ".*", $options: "i" } },
            { tellerInitial: { $regex: searchQuery + ".*", $options: "i" } },
            { tellerEmail: { $regex: searchQuery + ".*", $options: "i" } }]
        };
    },
    formatCompanyParams: function (params) {
        let attributeToRemove = ["isActive"];
        Object.keys(params).map(key => {
            if (attributeToRemove.includes(key)) {
                delete params[key];
            }
        });
        return params;
    }
}