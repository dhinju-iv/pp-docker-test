process.env.NODE_ENV = 'test';

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../app');
var should = chai.should();

var companyId = "";

chai.use(chaiHttp);

describe('Company', () => {
    describe('/CreateCompany', () => {
        it('It should not create a company without company name, company email', (done) => {
            var company = {}
            chai.request(server)
                .post('/create')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.be.a('object').have.property('error').be.a('object')
                        .have.property('validationErrors').be.a('object')
                        .have.property('companyName');
                    res.body.should.be.a('object').have.property('error').be.a('object')
                        .have.property('validationErrors').be.a('object')
                        .have.property('companyEmail');
                    res.body.should.have.property('status').eql(false);
                    done();
                });
        });
        it('It should not create a company with empty company name', (done) => {
            var company = {
                companyName: ""
            }
            chai.request(server)
                .post('/create')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.be.a('object').have.property('error').be.a('object')
                        .have.property('validationErrors').be.a('object')
                        .have.property('companyName');
                    res.body.should.be.a('object').have.property('error').be.a('object')
                        .have.property('validationErrors').be.a('object')
                        .have.property('companyEmail');
                    res.body.should.have.property('status').eql(false);
                    done();
                });
        });
        it('It should not create a company with non string company name', (done) => {
            var company = {
                companyName: 000
            }
            chai.request(server)
                .post('/create')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.be.a('object').have.property('error').be.a('object')
                        .have.property('validationErrors').be.a('object')
                        .have.property('companyName');
                    res.body.should.be.a('object').have.property('error').be.a('object')
                        .have.property('validationErrors').be.a('object')
                        .have.property('companyEmail');
                    res.body.should.have.property('status').eql(false);
                    done();
                });
        });
        it('It should not create a company without company email', (done) => {
            var company = {
                companyName: "IVS",
            }
            chai.request(server)
                .post('/create')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.be.a('object').have.property('error').be.a('object')
                        .have.property('validationErrors').be.a('object')
                        .have.property('companyEmail');
                    res.body.should.have.property('status').eql(false);
                    done();
                });
        });
        it('It should not create a company with non valid company email', (done) => {
            var company = {
                companyName: "IVS",
                companyEmail: "email"
            }
            chai.request(server)
                .post('/create')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.be.a('object').have.property('error').be.a('object')
                        .have.property('validationErrors').be.a('object')
                        .have.property('companyEmail');
                    res.body.should.have.property('status').eql(false);
                    done();
                });
        });
        it('It should not create a company with company Id as input', (done) => {
            var company = {
                companyId: "COMP0000",
                companyName: "IVS",
                companyEmail: "test@email.com"
            }
            chai.request(server)
                .post('/create')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object').have.property('error').be.a('object')
                        .have.property('validationErrors').be.a('object')
                        .have.property('companyId');
                    res.body.should.have.property('status').eql(false);
                    done();
                });
        });
        it('It should create a company', (done) => {
            var company = {
                companyName: "IVS",
                companyEmail: "test@email.com"
            }
            chai.request(server)
                .post('/create')
                .send(company)
                .end((err, res) => {
                    companyId = res.body.data.companyId;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eql(true);
                    done();
                });
        });
        it('It should not create a company with existing company name', (done) => {
            var company = {
                companyName: "IVS",
            }
            chai.request(server)
                .post('/create')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object').have.property('error').be.a('object')
                        .have.property('validationErrors').be.a('object')
                        .have.property('companyName');
                    res.body.should.have.property('status').eql(false);
                    done();
                });
        });
        it('It should not create a company with existing company email', (done) => {
            var company = {
                companyEmail: "test@email.com",
            }
            chai.request(server)
                .post('/create')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object').have.property('error').be.a('object')
                        .have.property('validationErrors').be.a('object')
                        .have.property('companyEmail');
                    res.body.should.have.property('status').eql(false);
                    done();
                });
        });
    });
    describe('/SearchCompany', () => {
        it('It should not search a company with empty company name', (done) => {
            var company = {
                companyName: ""
            }
            chai.request(server)
                .post('/search')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.be.a('object').have.property('error').be.a('object')
                        .have.property('validationErrors').be.a('object')
                        .have.property('companyName');
                    res.body.should.have.property('status').eql(false);
                    done();
                });
        });
        it('It should not search a company with non valid company email', (done) => {
            var company = {
                companyEmail: "email"
            }
            chai.request(server)
                .post('/search')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.be.a('object').have.property('error').be.a('object')
                        .have.property('validationErrors').be.a('object')
                        .have.property('companyEmail');
                    res.body.should.have.property('status').eql(false);
                    done();
                });
        });
        it('It should search a company with company name', (done) => {
            var company = {
                companyName: "IVS"
            }
            chai.request(server)
                .post('/search')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eql(true);
                    res.body.should.be.a('object').have.nested.property('data.companyId');
                    res.body.should.be.a('object').have.nested.property('data.companyName');
                    res.body.should.be.a('object').have.nested.property('data.companyEmail');
                    res.body.should.be.a('object').have.nested.property('data.isActive').eql(true);
                    done();
                });
        });
        it('It should search a company with company email', (done) => {
            var company = {
                companyEmail: "test@email.com"
            }
            chai.request(server)
                .post('/search')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eql(true);
                    res.body.should.be.a('object').have.nested.property('data.companyId');
                    res.body.should.be.a('object').have.nested.property('data.companyName');
                    res.body.should.be.a('object').have.nested.property('data.companyEmail');
                    res.body.should.be.a('object').have.nested.property('data.isActive').eql(true);
                    done();
                });
        });
        it('It should search a company with company id', (done) => {
            var company = {
                companyId: companyId
            }
            chai.request(server)
                .post('/search')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eql(true);
                    res.body.should.be.a('object').have.nested.property('data.companyId');
                    res.body.should.be.a('object').have.nested.property('data.companyName');
                    res.body.should.be.a('object').have.nested.property('data.companyEmail');
                    res.body.should.be.a('object').have.nested.property('data.isActive').eql(true);
                    done();
                });
        });
        it('It should search a company with search query', (done) => {
            var company = {
                searchQuery: "IV"
            }
            chai.request(server)
                .post('/search')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eql(true);
                    res.body.should.be.a('object').have.nested.property('data[0].companyId');
                    res.body.should.be.a('object').have.nested.property('data[0].companyName');
                    res.body.should.be.a('object').have.nested.property('data[0].companyEmail');
                    res.body.should.be.a('object').have.nested.property('data[0].isActive').eql(true);
                    done();
                });
        });
    });
    describe('/SetCompanyStatus', () => {
        it('It should not activate/deactivate a company with invalid company id', (done) => {
            var company = {
                companyId: "COMP0000"
            }
            chai.request(server)
                .post('/setCompanyStatus')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.be.a('object').have.property('error').be.a('object')
                        .have.property('validationErrors').be.a('object')
                        .have.property('companyId');
                    res.body.should.have.property('status').eql(false);
                    done();
                });
        });
        it('It should not activate/deactivate a company with non boolean active flag', (done) => {
            var company = {
                companyId: companyId,
                isActive: "false"
            }
            chai.request(server)
                .post('/setCompanyStatus')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.be.a('object').have.property('error').be.a('object')
                        .have.property('validationErrors').be.a('object')
                        .have.property('isActive');
                    res.body.should.have.property('status').eql(false);
                    done();
                });
        });
        it('It should activate/deactivate a company', (done) => {
            var company = {
                companyId: companyId,
                isActive: false
            }
            chai.request(server)
                .post('/setCompanyStatus')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eql(true);
                    done();
                });
        });
    });
    describe('/UpdateCompany', () => {
        it('It should not update a company without passing companyId', (done) => {
            var company = {}
            chai.request(server)
                .post('/update')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.be.a('object').have.property('error').be.a('object')
                        .have.property('validationErrors').be.a('object')
                        .have.property('companyId');
                    res.body.should.have.property('status').eql(false);
                    done();
                });
        });
        it('It should not update a company with wrong companyId', (done) => {
            var company = {
                companyId: "COMP0000"
            }
            chai.request(server)
                .post('/update')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.be.a('object').have.property('error').be.a('object')
                        .have.property('validationErrors').be.a('object')
                        .have.property('companyId');
                    res.body.should.have.property('status').eql(false);
                    done();
                });
        });
        it('It should not update a company with non string company name', (done) => {
            var company = {
                companyId: companyId,
                companyName: 000
            }
            chai.request(server)
                .post('/update')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.be.a('object').have.property('error').be.a('object')
                        .have.property('validationErrors').be.a('object')
                        .have.property('companyName');
                    res.body.should.have.property('status').eql(false);
                    done();
                });
        });
        it('It should not update a company with non valid company email', (done) => {
            var company = {
                companyId: companyId,
                companyEmail: "email"
            }
            chai.request(server)
                .post('/update')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.be.a('object').have.property('error').be.a('object')
                        .have.property('validationErrors').be.a('object')
                        .have.property('companyEmail');
                    res.body.should.have.property('status').eql(false);
                    done();
                });
        });
        it('It should update a company', (done) => {
            var company = {
                companyId: companyId,
                companyEmail: "test@email.com"
            }
            chai.request(server)
                .post('/update')
                .send(company)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eql(true);
                    done();
                });
        });
    });
});