var request = require('request');



function makedataservicerequestnetworkFunc(query, operation, table, method, selector, options, keyIdentifier){
    return new Promise ((resolve, reject) => {
        try {
            var reqQuery = {};
            var reqOptions = {};
            reqQuery.url = "mongodb://mongo:27017",
                reqQuery.table = table,
                reqQuery.query = query,
                reqQuery.db = "dcs_pp",
                reqQuery.options = options,
                reqQuery.selector = selector;
            if (keyIdentifier !== null) {
                reqQuery.keyIdentifier = keyIdentifier;
            }
            reqOptions.url = query.serviceUrl + operation;
            reqOptions.body = reqQuery;
            reqOptions.json = true;
            reqOptions.method = method;
            resolve(reqOptions)
        } catch (error) {
            reject({ status: false, data: null, error: "Can't connect to Data service" });
        }
    })
}

function makeHTTPRequestFunc(options){
    return new Promise ((resolve, reject) => {
        try {
            request(options, (error, response, body) => {
                if (error) {
                    resolve({ status: false, data: null, error: "Can't connect" });
                } else {
                    resolve(body);
                }
            });
        } catch (error) {
            reject({ status: false, data: null, error: "Can't connect" });
        }
    })
}

module.exports = {    

    makedataservicerequestnetwork: async (query, operation, table, method, selector, options, keyIdentifier, serviceUrl) => {
        let response = await makedataservicerequestnetworkFunc(query, operation, table, method, selector, options, keyIdentifier, serviceUrl)
        if(response){
            let requestResponse = await makeHTTPRequestFunc(response)
            return requestResponse 
        }
    }

}
