var MongoClient = require("mongodb").MongoClient,
    ObjectId = require("mongodb").ObjectID;

function connectDB(url, dbName, callBack) {
    "use strict";
    MongoClient.connect(url, { useNewUrlParser: true }, function (err, db) {
        if (err) {
            callBack(null, null, err);
        } else {
            callBack(db.db(dbName), db, null);
        }
    });
}

function connect(cnString, dbName, callBack) {
    "use strict";
    try {
        connectDB(cnString, dbName, function (db, dbobj, err) {
            callBack(db, dbobj, err);
        });
    } catch (ex) {
        callBack(ex, null);
    }
}

module.exports.insertDocument = function (cnString, dbName, tabl, data, callback) {
    "use strict";
    try {
        connect(cnString, dbName, function (db, dbobj, err) {
            if (!err) {
                var collection = db.collection(tabl);
                collection.insertOne(data, function (err, result) {
                    dbobj.close();
                    if (!err) {
                        callback(null, result);
                    } else {
                        callback(err, null);
                    }
                });
            } else {
                callback(err, null);
            }
        });
    } catch (ex) {
        callback(ex, null);
    }
};

module.exports.updateDocument = function (cnString, dbName, tabl, data, callback, keyIdentifier) {
    "use strict";
    try {
        connect(cnString, dbName, function (db, dbobj, err) {
            if (!err) {
                var collection = db.collection(tabl);
                if (keyIdentifier) {
                    var id = keyIdentifier;
                    var q = {};
                    q[id] = data[id];
                    if (data._id) {
                        delete data._id
                    }
                    var updateDoc = { $set: data };
                    collection.updateOne(q, updateDoc, { upsert: true }, function (err, result) {
                        dbobj.close();
                        if (!err) {
                            callback(null, result);
                        } else {
                            callback(err, null);
                        }
                    });
                } else {
                    var id = new ObjectId(data._id);
                    delete data._id;
                    var updateDoc = { $set: data };
                    collection.updateOne({ _id: id }, updateDoc, function (err, result) {
                        if (!err) {
                            callback(null, result);
                        } else {
                            callback(err, null);
                        }
                    });
                }
            } else {
                callback(err, null);
            }
        });
    } catch (ex) {
        callback(ex, null);
    }
};


module.exports.insertDocuments = function (cnString, dbName, tabl, data, callback) {
    "use strict";
    try {
        connect(cnString, dbName, function (db, dbobj, err) {
            if (!err) {
                var collection = db.collection(tabl);
                collection.insertMany(data, function (err, result) {
                    dbobj.close();
                    if (!err) {
                        callback(result, null);
                    } else {
                        callback(null, err);
                    }
                });
            } else {
                callback(err, null);
            }
        });
    } catch (ex) {
        callback(ex, null);
    }
};

module.exports.findDocuments = function (cnString, dbName, tbl, query, callback, selector, limit, order) {
    "use strict";
    try {
        connect(cnString, dbName, function (mdb, dbobj, err) {
            if (!err) {
                var collection = mdb.collection(tbl);
                collection.find(query, selector).collation({ locale: 'en_US', numericOrdering: true }).limit(limit).sort(order).toArray(function (err, docs) {
                    dbobj.close();
                    if (!err) {
                        callback(null, docs);
                    } else {
                        callback(err, null);
                    }
                });
            }
        });
    } catch (ex) {
        callback(ex, null);
    }
};

module.exports.findDocument = function (cnString, dbName, tbl, query, callback, selector) {
    "use strict";
    try {
        connect(cnString, dbName, function (db, dbobj, err) {
            if (!err) {
                var collection = db.collection(tbl);
                collection.findOne(query, selector, function (err, docs) {
                    // db.close();
                    dbobj.close();
                    if (!err) {
                        callback(null, docs);
                    } else {
                        callback(err, null);
                    }
                });
            } else {
                callback(err, null);
            }
        });
    } catch (ex) {
        callback(ex, null);
    }
};

module.exports.getCount = function (cnString, dbName, tbl, query, callback) {
    "use strict";
    try {
        connect(cnString, dbName, function (db, dbobj, err) {
            if (!err) {
                var collection = db.collection(tbl);
                collection.countDocuments(query, function (err, count) {
                    dbobj.close();
                    if (!err) {
                        callback(null, count);
                    } else {
                        callback(err, null);
                    }
                });
            }
        });
    } catch (ex) {
        callback(ex, null);
    }
};

module.exports.aggregate = function (cnString, dbName, tbl, query, callback) {
    "use strict";
    try {
        connect(cnString, dbName, function (db, dbobj, err) {
            if (!err) {
                var collection = db.collection(tbl);
                collection.aggregate(query).toArray(function (err, docs) {
                    dbobj.close();
                    if (!err) {
                        callback(null, docs);
                    } else {
                        callback(err, null);
                    }
                });
            }
        });
    } catch (ex) {
        callback(null, ex);
    }
};