var express = require('express'),
    bodyParser = require('body-parser'),
    compress = require('compression');

process.env.NODE_DEBUG = "net http";

var indexRouter = require('./routes/index');
var app = express();

app.use(compress());
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

app.use('/', indexRouter);

app.use(function (req, res, next) {
    return res.status(404).send({ status: false, data: null, error: "Invalid Path" });
});

app.use((err, req, res, next) => {
    if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
        return res.status(400).send({ status: false, data: null, error: "Bad request" });
    }
    next();
});

var port = 3007;
app.set('port', port);
app.listen(port);