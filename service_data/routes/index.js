var express = require('express');
var controller = require("../controller/index");

var router = express.Router();

router.post('/read', (req, res) => {
    try {
        'use strict';
        var query = req.body;
        controller.readData(query, function (err, data){
            if (err) {
                res.send({ status: false, data: data, error: err.message });
            } else {
                res.send({ status: true, data: data, error: err });
            }
        });
    } catch (err) {
        res.send({ status: false, data: null, error: "Can't get any result" });
    }
});

router.post('/write', (req, res) => {
    try {
        'use strict';
        var query = req.body;
        controller.writeData(query, function (err, data){
            if (err) {
                res.send({ status: false, data: data, error: err.message });
            } else {
                res.send({ status: true, data: data, error: err });
            }
        });
    } catch (err) {
        res.send({ status: false, data: null, error: "Can't get any result" });
    }
});

router.post('/count', (req, res) => {
    try {
        'use strict';
        var query = req.body;
        controller.getCount(query, function (err, data){
            if (err) {
                res.send({ status: false, data: data, error: err.message });
            } else {
                res.send({ status: true, data: data, error: err });
            }
        });
    } catch (err) {
        res.send({ status: false, data: null, error: "Can't get any result" });
    }
});

router.post('/aggregate', (req, res) => {
    try {
        'use strict';
        var query = req.body;
        controller.aggregate(query, function (err, data){
            if (err) {
                res.send({ status: false, data: data, error: err.message });
            } else {
                res.send({ status: true, data: data, error: err });
            }
        });
    } catch (err) {
        res.send({ status: false, data: null, error: "Can't get any result" });
    }
});

module.exports = router;