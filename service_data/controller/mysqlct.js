
module.exports = function (params) {
    var app = params.app;

    var sendResponse = function (res, data, err) {
        res.send(JSON.stringify({
            statusCode: -100,
            statusMessage: data,
            errorMessage: err,
            iid: (data !== null) ? data.insertedId : null
        }));
    };

    app.post('/readMySQL', function (req, res) {
        'use strict';
        console.log("calling read mysql");
        var query = req.body;
        //console.log(query);
        var sqlMY = require('../datasource/mysqlcli.js');
        sqlMY.select(query.source, query.query, function (error, results, fields) {
            //console.log(error,results,fields);
            sendResponse(res, error, results);
        });
    });
};